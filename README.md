# Handover prep
 `16.07.2019`
 
## Gotchas
| current situation | recommendations (/) | possible repercussions (if not fixed) |
---|---|---
|	(-) datascience workflow has no clear documentation of how the database(s) evolves |- use version control to keep track of DB updates i.e. schema changes, joins, views, etc.| - Engineering support required every time a "release" of the database is prepared by DS<br>** very cost-/labor-intensive, proven time and again since project start 
|   (!) [long-lived branches](#branches) | - integrate often<br />-  keep PR cycles short | - massive merge conflicts, difficult to reconcile
|	(!) [major/minor] releases are not tagged | - implement CI workflow incl. tagging step in the `master` branch | - worst-case: incremental roll out of changes cannot be applied, full teardown and re-setup  required, <br>- Given: local changes may have been applied manually without a commit against the repository, there is no straightforward way to reconcile differences i.e. no clean rollback when commits aren't tagged
|   (!) min. documentation of [tech. debt](#technical-debt) | - include `ToDo/Fixme`s to source code if ticket does not allow for immediate resolution<br>- regular review of peppered `ToDo/FixMe`s, translate them into tickets and prioritize | - cleanup of [galaxy-expansion] has been massively affected by this issue (originally estimated at 13 StoryPoints, actual effort may now very well be 42) 
|	database migration done semi-automatically with (via snapshot-sharing and/or python?) scripts | - use AWS database migration service<br>-- (+) standardized CDC process, switching on/off VPC peering between accounts and replication can be done with terraform | - has been a recurring issue (and may continue) to be dealt with aggravated by datascience workflow<br>- current quickfix strategy may/not be sufficient for future issues |
|   further cost-optimization on resources can be done | - bastion: elastic IP, auto-scale group of 1 & spot-instance, this setup allows you avoid updating the `bastion-ssh-keys/localconfig` settings everytime the IP address of the bastion host changes<br>- switch to reserved instances for all the other ec2 instance needs | suboptimal spending
|   terraform module versions are inconsistent | - quickfix: see related tagging issue, standardize versioning i.e. `v0.0.0` vs `tf-0.11.14-0` <br>- Long-term: consider using modules from Terraform [module registry] where possible | 
|	ec2 instances other than the bastion are accessible from the outside the VPC i.e. multiple bastions | - quickfix: "access-from-everywhere" policy is only attached temporarily during development<br>- long-term: stable configurations of instances should be triggered from within the VPC<br>- avoid using the same key pair for all the instances to minimize vulnerable surface area | 
|   current state of prod has been partially rolled-out manually | - re-roll out production once testing phase is done | - currently not able to pinpoint which commit was used in the production roll out<br>- unable to verify if there exists a commit pertaining to what is currently rolled out in the prod account

---
### Branches
[`webapp-frontend-milky-way`][webapp-frontend-milky-way]

![branches](branches.png)

---
### Technical Debt
[`galaxy-expansion`][galaxy-expansion]

![tech-debt](tech-debt.png)
 
 
## Repo overview
|project | # of repositories
---|---
 **TOTAL**                | `55`
 [auxiliary][AUX]         | `2` (Coding challenges)
 [datascience][DAT]		  | `3`  (models, python-setup)
 [infrastructure][IN]     | `4` (infrastructure) <br/>`25` (terraform modules)
 [open-source][OP]        | `1` (pre-commit i.e. terraform linter/fmt)
 [pipelines][PIP]		  | `6` (data processing)
 [services][SER]		  | `6` (docker images) <br/> `2` (templates)
 [webapp][WA]             | `6` 

[galaxy-expansion]: https://bitbucket.org/geanalytics/infrastructure-galaxy-expansion/pull-requests/20/release-ge-554-god/diff
[webapp-frontend-milky-way]: https://bitbucket.org/geanalytics/webapp-frontend-milky-way/branches/
[module registry]: https://registry.terraform.io/
[xloader]: https://bitbucket.org/geanalytics/xloader-pipeline/src/master/
[AUX]: https://bitbucket.org/account/user/geanalytics/projects/AUX
[DAT]: https://bitbucket.org/account/user/geanalytics/projects/DAT
[IN]: https://bitbucket.org/account/user/geanalytics/projects/IN
[OP]: https://bitbucket.org/account/user/geanalytics/projects/OP
[PIP]: https://bitbucket.org/account/user/geanalytics/projects/PIP
[SER]: https://bitbucket.org/account/user/geanalytics/projects/SER
[WA]: https://bitbucket.org/account/user/geanalytics/projects/WA